# polycode-jobs

This repository is a part of [polycode project](https://gitlab.polytech.umontpellier.fr/polycode1).

It contains jobs used for the CI/CD pipelines.

As those jobs are hosted on **[r2devops](r2devops.io)**, this repository needed to be public and hosted on gitlab.com.