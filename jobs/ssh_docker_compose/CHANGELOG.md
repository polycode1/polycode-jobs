# Changelog
All notable changes to this job will be documented in this file.

## [1.0.2] - 2022-22-05
* Fix file variables

## [1.0.1] - 2022-22-05
* Fix temporary directory

## [1.0.0] - 2022-22-05
* Initial version