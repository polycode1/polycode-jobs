## Objective

Connect through ssh inside a server, copy environnement variables and retrieve a Docker compose file. The environnement variables are then sourced and the Docker compose file is executed. 

## How to use it

1. Add this job URL inside the `include` list of your `.gitlab-ci.yml` file (see the [quick setup](/use-the-hub/#quick-setup)). You can specify [a fixed version](#changelog) instead of `latest`.
    ```yaml
      - remote: 'https://jobs.r2devops.io/latest/ssh_docker_compose.yml'
    ```
1. If you need to customize the job (stage, variables, ...) 👉 check the [jobs
   customization](/use-the-hub/#jobs-customization)
1. Well done, your job is ready to work ! 😀

## Job details

* Job name: `ssh_docker_compose`
* Docker image:
[`alpine:3.15`](https://hub.docker.com/r/_/alpine)
* Default stage: `deploy`
* When: `always`

### Variables

| Name | Description | Default |
| ---- | ----------- | ------- |
| `PRIVATE_SSH_KEY` <img width=100/> | Private key to connect to the server <img width=175/>| `` <img width=100/>|
| `SERVER_USERNAME` <img width=100/> | username to connect to the server <img width=175/>| `` <img width=100/>|
| `SERVER_IP` <img width=100/> | Public IP address the server <img width=175/>| `` <img width=100/>|
| `DOCKER_COMPOSE_FILE_URL` <img width=100/> | The docker compose file to retrieve. (is a snippet code here) <img width=175/>| `` <img width=100/>|
| `DOCKER_ENV_VARIABLES` <img width=100/> | File to put variables relative to Docker <img width=175/>| `` <img width=100/>|


!!! warning 
    As they are confidential, those variables should be set in the CI, CD settings of the project instead of the `.gitlab-ci.yml` file.

## Requirement

- [docker-compose](https://docs.docker.com/compose/install/) (server)
- curl (server)